import { Component } from '@angular/core';
import {InformacionService} from '../../services/informacion.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {

  /*Con esto ya tengo accesso a todos los metodos y propiedades de informacionservice*/
  constructor(public _is:InformacionService){}


}
